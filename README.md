# Dokumentation

Die Bots bestehen aus diesen Komponenten:

- GitLab CI Konfiguration ([Beispiel](https://gitlab.com/fonflatter/bots/comic/blob/master/.gitlab-ci.yml), [Dokumentation](https://docs.gitlab.com/ee/ci/yaml/))
- Transform-Script, dass Inhalt zusammen bastelt ([Beispiel](https://gitlab.com/fonflatter/bots/comic/blob/master/transform.js))
- Scripte, die Inhalt verbreiten
  - Twitter: https://gitlab.com/winniehell/easy-tweet/

Die **GitLab CI Konfiguration** hat diese Bestandteile:

- `download`-Job:  
  Er läd Dinge von https://fonflatter.de herunter und stopft sie in das Transform-Script.  
  Dabei purzelt eine `payload.json` heraus.
- ein Job pro Medium (z.B. `twitter`):  
  Schnappt sich die gepurzelte `payload.json` und verbreitet den Inhalt.  
  Dafür wird pro Medium ein eigenes Script verwendete, das jeweils in einem separaten Projekt liegt.  
  Für den Login benötigt es Umgebungsvariablen wie beispielsweise `TWITTER_SECRETS` ([Konfiguration](https://gitlab.com/groups/fonflatter/bots/-/settings/ci_cd), [Dokumentation](https://docs.gitlab.com/ee/ci/variables/))
- jeweils einen Test-Job zum Testen

Eine neue Pipeline mit den ensprechenden Jobs wird durch Pipeline Schedules gestartet ([Beispiel](https://gitlab.com/fonflatter/bots/comic/pipeline_schedules), [Dokumentation](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html)).

Das **Transform-Script** erhält Daten von fonflatter.de ([Beispiel](https://test.fonflatter.de/api/comics/2015-07-03)) und wandelt sie ein ein verbreitbares Format um ([Beispiel](https://gitlab.com/winniehell/easy-tweet/blob/master/test/media/payload.json)).

Das Ganze funktioniert dann ungefähr so:

```mermaid
graph LR;
  A(Montag) -- macht --> B[geschedulte Pipeline];
  B -- startet --> C[download-Job];
  C -- startet --> C0[Transform-Script];
  D(fonflatter.de) -- liefert Dinge an --> C;
  C0 -- übergibt payload.json an --> E[twitter-Job];
  F[Umgebungsvariablen] -- verraten Passwort an --> E;
  E -- startet --> G[easy-tweet Script];
  G -- zwitschert ins --> H(Twitter);
```

## Zeiten anpassen

![Zeiten anpassen](zeiten-anpassen.mp4)